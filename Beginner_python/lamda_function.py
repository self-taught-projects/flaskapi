def add(x, y):
    return x + y

# Lamda function
add_lamda = lambda x, y: x + y
print(add_lamda(5,5))

# Lamda callback function 
print((lambda x, y: x * y)(5,5))