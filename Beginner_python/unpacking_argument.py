def multiply(*args):
    print(args)

multiply(1,3,4)

def add(x, y):
    return x + y

num = [3, 5]
print(add(*num))

def multiply_operation(*args):
    total = 1
    for arg in args:
        total = total * arg
    return total

def apply(*args, operator):
    if operator == '*':
        return multiply_operation(*args)
    elif operator == '+':
        return sum(args)
    else:
        return 'No valid operator provided to apply()'

print(apply(1, 3, 6, 7, operator='*'))