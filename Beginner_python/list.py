numbers = [1, 3, 5]
doubled = []
# Old style for creating a new list
for num in numbers:
    doubled.append(num * 2)

doubled_list = [num *2 for num in numbers]

friends = ['Rolf', 'Sam', 'Samantha', 'Saurabh', 'Jen']
starts_s = []
# Old style
for friend in friends:
    if friend.startswith('S'):
        starts_s.append(friend)

# List comprehension
starts_s_comprehension = [friend for friend in friends if friend.startswith('S')]

