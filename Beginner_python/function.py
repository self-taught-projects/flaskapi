def hello(name):
    print(f'Hello, {name}')
hello('Dara')

def user_age_in_seconds():
    user_age = int(input('Enter your age'))
    age_seconds = user_age * 365 * 24 * 60 * 60
    print(f'Your age in seconds is {age_seconds}')

user_age_in_seconds()

def add(x, y):
    print(x * y)

add(5, 5)

def say_hello():
    print('Hello!')

say_hello('Dara')