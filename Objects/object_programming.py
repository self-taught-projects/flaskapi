#student = {'name': 'Rolf', 'grades': (89, 90, 93, 78, 90)}


# def average(sequence):
# return sum(sequence) / len(sequence)


# print(average(student['grades']))
# print(student.average())


class Student:
    def __init__(self, name, grades):
        self.name = name
        self.grades = grades
    def average(self):
        return sum(self.grades) / len(self.grades)

student_one = Student('Bob', (90, 90, 93, 78, 90))
student_two = Student('Dara', (90, 90, 93, 78, 90))
print(student_two.average())
print(student_one.average())
